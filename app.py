from flask import Flask, render_template,url_for,request
import snowflake.connector

app=Flask(__name__)

ctx = snowflake.connector.connect(
    user='vijay',
    password='vijaySN@12',
    account='wm25129.ap-south-1.aws',
    warehouse='compute_wh',
    database='sampledb',
    schema='public',
    )

@app.route('/',methods=['POST','GET'])
def index():
	one_row=[]
	cs = ctx.cursor()
	if request.method=='POST':
		v=request.form['name']
		if len(v)<2:
			return render_template('index.html',error="please enter valid details")
		q="select * from sample_tab where name='"+v+"';"
		try:
		    # cs.execute("INSERT INTO sample_python VALUES('syam',18);") #optional
		    # cs.execute("INSERT INTO sample_python VALUES('unika',20);") #optional
		    cs.execute(q)
		    one_row = cs.fetchall()
		finally:
		    cs.close()
		return render_template('index.html',users=one_row,heads=['Name','Age'])
	else:
		return render_template('index.html',error="person not found")
	return render_template('index.html',error="please search for users with name")

@app.route('/demo/<string:hsp>')
def second(hsp):
	css=ctx.cursor()
	q="SELECT * FROM SCHOOL WHERE name='"+str(hsp)+"';"
	css.execute(q)
	sch=css.fetchone()
	css.close()
	return "hai! "+hsp+" school is "+sch[1]


#TO CLOSE CONNECTION
# ctx.close()

if __name__=="__main__":
	app.run(debug=True)